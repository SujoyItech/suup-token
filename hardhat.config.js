require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config();

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  networks: {
    goerlie: {
      url: process.env.RPC_URL, // Update the port if necessary
      accounts: [process.env.PRIVATE_KEY]
    },
  },
  solidity: "0.8.17",
};
