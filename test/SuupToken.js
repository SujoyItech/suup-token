const {expect} = require('chai');
const hre = require('hardhat');

describe('SuppToken Contract', ()=> {
    // global vars
    let Token;
    let suupToken;
    let owner;
    let addr1;
    let addr2;
    let tokenCap = 100000000;
    let tokenBlockReward = 50;

    beforeEach( async ()=> {
        [owner, addr1, addr2] = await ethers.getSigners();
        Token = await ethers.getContractFactory("SuupToken");
        suupToken = await Token.deploy(tokenCap, tokenBlockReward);
    });

    describe("Deployment", ()=> {
        it('Should set the right owner', async ()=> {
            expect(await suupToken.owner()).to.equal(owner.address);
        });

        it('Should assign the total supply of tokens to the owner', async ()=> {
            const ownerBalance = await suupToken.balanceOf(owner.address);
            expect(await suupToken.totalSupply()).to.equal(ownerBalance);
        });

        it('Should set the max capped supply to the argument provided during deployment', async ()=> {
            const cap = await suupToken.cap();
            expect(Number(ethers.formatEther(cap))).to.equal(tokenCap);
        });
        
        it('Should set the blockReward to the argument provided during deployment', async ()=> {
            const blockReward = await suupToken.blockReward();
            expect(Number(ethers.formatEther(blockReward))).to.equal(tokenBlockReward);
        });
    });

    describe("Transactions", ()=> {
        it('Should transfer token between accounts', async ()=> {
            // Transfer 50 tokens from owner to addr1
            await suupToken.transfer(addr1.address, 50);
            const addr1Blance = await suupToken.balanceOf(addr1.address);
            expect(addr1Blance).to.equal(50);

            // Transfer 50 tokens from addr1 to addr2
            // We use .connect(signer) to send a transaction from another account
            await suupToken.connect(addr1.address).transfer(addr2.address, 50);
            const addr2Balance = await suupToken.balanceOf(addr2.address);
            expect(addr2Balance).to.equal(50);
        });

        it("Should fail if sender doesn't have enough tokens", async ()=> {
            const initalOwnerBalance = await suupToken.balanceOf(owner.address);

            // Try to send 1 token from addr (0 token) to owner (10000000 tokens),
            // require will evaluate false and revert the transaction

            await expect(
                await suupToken.connect(addr1.address).transfer(owner.address, 1)
            ).to.be.revertedWith("ERC20: transfer amount exceeds balance");

            // Owner balance should not have changed
            expect(
                await suupToken.balanceOf(owner.address).to.equal(initalOwnerBalance)
            )
        });

        it('Should update balance after transfer', async()=> {
            const initialOwnerBlance = await suupToken.balanceOf(owner.address);

            // Transfer 100 tokens from owner to addr1
            await suupToken.transfer(addr1.address, 100);

            // Transfer another 50 tokens from owner to addr2
            await suupToken.transfer(addr2.address, 50);

            // Check balances
            const finalOwnerBalance = await suupToken.balanceOf(owner.address);
            expect(finalOwnerBalance).to.equal(initialOwnerBlance.sub(150));

            // Addr1 balance 
            const addr1Balance = await suupToken.balanceOf(addr1.address);
            expect(addr1Balance).to.equal(100);

            // Addr2 balance
            const addr2Balance = await  suupToken.balanceOf(addr2.address);
            expect(addr2Balance).to.equal(50);

        });
        
    })
})